FROM alpine:edge
RUN apk update \
	&& apk --no-cache add dnsmasq \
	&& apk add --no-cache --virtual .build-deps curl \
	&& apk del .build-deps
RUN mkdir -p /etc/default/ && echo -e "ENABLED=1\nIGNORE_RESOLVCONF=yes" > /etc/default/dnsmasq
ENTRYPOINT ["dnsmasq","--no-daemon"]
