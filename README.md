# MinimalDNS

MinimalDNS sets up a docker container exposing a minimal DNS server.

Comfiguration is done through an /etc/hosts file, in it you can link a domain to an IP address.

## Getting started

```bash
git clone https://gitlab.com/ricargoes/minimaldns.git
cd minimaldns
cp /etc/hosts etc-hosts
docker-compose up -d
```
If you do not want to use your /etc/hosts file, just edit etc-hosts as you see fit
